package com.nau.lab6;

import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

public class ButtonsBlock {
    private final RadioButton start;
    private final RadioButton stop;

    public ButtonsBlock() {
        ToggleGroup toggleGroup = new ToggleGroup();
        start = new RadioButton("Start");
        stop = new RadioButton("Stop");

        start.setToggleGroup(toggleGroup);
        stop.setToggleGroup(toggleGroup);
    }

    public RadioButton getStop() {
        return stop;
    }

    public RadioButton getStart() {
        return start;
    }

}
