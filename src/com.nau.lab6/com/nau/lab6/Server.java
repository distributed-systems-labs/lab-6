package com.nau.lab6;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

public class Server extends Application {
    private final Display display = new Display();
    private final Text numberCon = new Text("" + 0);
    private CustomRectangle[] rectangles;
    private ServerSocket serverSocket;
    private Text[] textArray;
    private Text resultText;

    private Group buildGeneralResult() {
        Group group = new Group();
        Text threadNum = createText("Thread 5", 12, Color.GRAY);
        resultText = createText("Общая площадь: 0 пикселов", 12, Color.TOMATO);
        resultText.setX(60);
        group.getChildren().addAll(threadNum, resultText);
        group.setLayoutX(60);
        group.setLayoutY(280);
        return group;
    }

    private Pane buildRectangleGroup() {
        GridPane pane = new GridPane();
        pane.setHgap(20);
        pane.setVgap(10);

        CustomRectangle r1 = new CustomRectangle(40, 130, Color.TOMATO);
        Text s1 = createText("0", 12, Color.TOMATO);
        Text t1 = createText("Thread 1", 12, Color.GRAY);
        CustomRectangle r2 = new CustomRectangle(130, 130, Color.GREEN);
        Text s2 = createText("0", 12, Color.GREEN);
        Text t2 = createText("Thread 2", 12, Color.GRAY);
        CustomRectangle r3 = new CustomRectangle(220, 130, Color.BLUE);
        Text s3 = createText("0", 12, Color.BLUE);
        Text t3 = createText("Thread 3", 12, Color.GRAY);
        CustomRectangle r4 = new CustomRectangle(310, 130, Color.ORANGE);
        Text s4 = createText("0", 12, Color.ORANGE);
        Text t4 = createText("Thread 4", 12, Color.GRAY);

        Rectangle serviceRectangle = new Rectangle(90, 130, 0, 140);

        rectangles = new CustomRectangle[]{r1, r2, r3, r4};
        textArray = new Text[]{s1, s2, s3, s4};

        pane.add(s1, 0, 0);
        pane.add(t1, 0, 1);
        pane.add(r1, 0, 2);

        pane.add(serviceRectangle, 1, 2);

        pane.add(s2, 2, 0);
        pane.add(t2, 2, 1);
        pane.add(r2, 2, 2);

        pane.add(s3, 4, 0);
        pane.add(t3, 4, 1);
        pane.add(r3, 4, 2);

        pane.add(s4, 6, 0);
        pane.add(t4, 6, 1);
        pane.add(r4, 6, 2);

        return pane;
    }

    private Text createText(String message, int size, Color color) {
        Text text = new Text(message);
        text.setFont(Font.font("Arial", FontWeight.NORMAL, size));
        text.setFill(color);
        return text;
    }

    private Text buildTitle() {
        Text title = createText("Площади прямоугольников в пикселах", 16, Color.TOMATO);
        title.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        title.setFill(Color.TOMATO);
        return title;
    }

    private Pane buildDisplay() {
        Pane pane = new Pane();
        Text port = new Text("Port: " + Constant.PORT);
        Text connections = new Text("Connections:");

        port.setLayoutY(40);
        port.setLayoutX(0);
        connections.setLayoutX(110);
        connections.setLayoutY(40);
        numberCon.setLayoutX(190);
        numberCon.setLayoutY(40);

        pane.setLayoutX(400);
        pane.setLayoutY(30);

        pane.getChildren().addAll(display, port, connections, numberCon);
        return pane;
    }

    private Pane buildRootPane() {
        Pane rootPane = new Pane();
        Text title = buildTitle();
        Pane rectanglesPane = buildRectangleGroup();
        Group result = buildGeneralResult();
        Pane displayPane = buildDisplay();

        GridPane mainPane = new GridPane();


        mainPane.setHgap(20);
        mainPane.setVgap(10);
        mainPane.setPadding(new Insets(10, 0, 0, 20));
        mainPane.add(title, 1, 0);
        mainPane.add(rectanglesPane, 1, 3);

        rootPane.getChildren().addAll(mainPane, result, displayPane);
        return rootPane;
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("Лабораторная работа №6. == Server == Кузнєцов Олексій");
        primaryStage.setResizable(false);

        Pane rootPane = buildRootPane();

        Scene scene = new Scene(rootPane, 650, 320, Color.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.show();

        ArrayList<RectangleTask> tasks = new ArrayList<>();
        ArrayList<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            RectangleTask task = new RectangleTask(rectangles[i], textArray[i]);
            Thread thread = new Thread(task);

            thread.setName("Thread " + i + 1);
            thread.start();
            tasks.add(task);
            threads.add(thread);
        }

        ResultTask resultTask = new ResultTask(resultText, tasks);
        Thread resultThread = new Thread(resultTask);
        resultThread.setName("Thread 5");
        resultThread.start();

        serverSocket = new ServerSocket(Constant.PORT);
        ServerTask mainTask = new ServerTask(serverSocket, display, numberCon, threads, tasks, resultTask);

        Thread mainThread = new Thread(mainTask);
        mainThread.start();

        primaryStage.setOnCloseRequest(event -> {
            mainThread.interrupt();
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("exit");
            System.exit(0);
        });

    }

    public static void main(String[] args) {
        launch(args);
    }

}
