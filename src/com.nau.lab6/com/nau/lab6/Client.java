package com.nau.lab6;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.Socket;


public class Client extends Application {
    private final Display display = new Display();
    private final TextField portTf = new TextField();
    private final TextField hostTf = new TextField();
    private final Button connectBtn = new Button("Connect");
    private final Button disconnectBtn = new Button("Disconnect");
    private final Title squareSum = new Title("0.0", Color.TOMATO);
    private final ButtonsBlock[] buttonsBlocks = new ButtonsBlock[]{new ButtonsBlock(), new ButtonsBlock(), new ButtonsBlock(), new ButtonsBlock()};
    private Thread mainThread = null;
    private Socket socket = null;
    private ClientTask task = null;
    private final Title[] squares = new Title[]{
        new Title("0.0", Color.TOMATO),
        new Title("0.0", Color.GREEN),
        new Title("0.0", Color.BLUE),
        new Title("0.0", Color.ORANGE),
    };


    private GridPane buildInputs() {
        GridPane gp = new GridPane();

        gp.setHgap(20);
        gp.setVgap(15);
        gp.setLayoutX(250);
        gp.setLayoutY(60);

        Text portText = new Text("Port:");
        Text hostText = new Text("Host:");
        portTf.setMaxWidth(100);
        portTf.setText("" + Constant.PORT);
        hostTf.setMaxWidth(100);
        hostTf.setText(Constant.HOST);
        connectBtn.setMinWidth(80);
        connectBtn.setTextFill(Color.TOMATO);
        connectBtn.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        disconnectBtn.setMinWidth(80);
        disconnectBtn.setTextFill(Color.TOMATO);
        disconnectBtn.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        disconnectBtn.setDisable(true);

        gp.add(portText, 0, 0);
        gp.add(portTf, 1, 0);
        gp.add(connectBtn, 4, 0);

        gp.add(hostText, 0, 1);
        gp.add(hostTf, 1, 1);
        gp.add(disconnectBtn, 4, 1);

        return gp;
    }

    private Pane buildBottomPane() {
        Pane mainPane = new Pane();
        Title resultTitle = new Title("Общая площадь:", Color.TOMATO);
        Text squareTitle = new Text("Площади прямоугольников:");

        mainPane.setLayoutY(180);

        squareSum.setLayoutY(70);
        squareSum.setLayoutX(50);
        squareTitle.setLayoutY(140);
        squareTitle.setLayoutX(40);

        GridPane controlPane = new GridPane();
        controlPane.setHgap(30);
        controlPane.setVgap(20);
        controlPane.setLayoutX(250);
        controlPane.setLayoutY(20);

        for (int i = 0; i < 4; i++) {
            controlPane.add(new Title("Thread " + (i + 1), Color.GRAY, FontWeight.NORMAL), i, 0);
            controlPane.add(buttonsBlocks[i].getStart(), i, 1);
            controlPane.add(buttonsBlocks[i].getStop(), i, 2);
            controlPane.add(squares[i], i, 3);
        }

        mainPane.getChildren()
            .addAll(
                resultTitle,
                squareSum,
                squareTitle,
                controlPane
            );
        return mainPane;
    }

    private Pane buildRootPane() {
        Pane mainPane = new Pane();
        display.setLayoutX(20);
        display.setLayoutY(40);

        display.setMaxWidth(200);
        display.setMaxHeight(120);

        Separator separator = new Separator(Orientation.HORIZONTAL);
        separator.setLayoutY(180);
        separator.setLayoutX(20);
        separator.setMinWidth(570);
        separator.setValignment(VPos.CENTER);
        separator.setVisible(true);

        mainPane.getChildren()
            .addAll(
                    new Title("Client"),
                    display,
                    separator,
                    buildInputs(),
                    buildBottomPane()
            );
        return mainPane;
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Лабораторная работа №6. == Client == Кузнєцов Олексій");
        primaryStage.setResizable(false);

        Pane rootPane = buildRootPane();

        Scene scene = new Scene(rootPane, 610, 360, Color.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.show();

        connectBtn.setOnAction((ActionEvent event) -> {
            task = new ClientTask(
                hostTf.getText(),
                portTf.getText(),
                display,
                connectBtn,
                disconnectBtn,
                buttonsBlocks,
                squares,
                squareSum
            );
            mainThread = new Thread(task);
            mainThread.start();
            disconnectBtn.setDisable(false);
            connectBtn.setDisable(true);
        });

        disconnectBtn.setOnAction((ActionEvent event) -> {
            mainThread.interrupt();
            disconnectBtn.setDisable(true);
            connectBtn.setDisable(false);
            try {
                socket = task.getSocket();
                socket.close();
                display.print("<<< Disconnected: " + hostTf.getText() + ":" + portTf.getText());
            } catch (IOException e) {
                e.printStackTrace();
                display.print("== Error during closing connection.");
            }
        });

        primaryStage.setOnCloseRequest(event -> {
            mainThread.interrupt();

            System.out.println("exit");
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
