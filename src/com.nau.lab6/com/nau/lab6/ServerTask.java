package com.nau.lab6;

import javafx.concurrent.Task;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ServerTask extends Task {
    private ServerSocket serverSocket;
    private final Display display;
    private final Text connectionNumber;
    private final ArrayList<Thread> threads;
    private final ArrayList<RectangleTask> rectangleTasks;
    private final ResultTask resultTask;

    public ServerTask(
        ServerSocket serverSocket,
        Display display,
        Text connectionNumber,
        ArrayList<Thread> threads,
        ArrayList<RectangleTask> rectangleTasks,
        ResultTask resultTask
    ) {
        this.serverSocket = serverSocket;
        this.display = display;
        this.connectionNumber = connectionNumber;
        this.threads = threads;
        this.rectangleTasks = rectangleTasks;
        this.resultTask = resultTask;
    }

    @Override
    protected Object call() {
        try {
            while (true) {
                Socket socket = serverSocket.accept();
                new WorkerThread(
                    socket,
                    display,
                    connectionNumber,
                    threads,
                    rectangleTasks,
                    resultTask
                ).start();
            }
        } catch (IOException e) {
            this.cancel();
        }
        return null;
    }
}
