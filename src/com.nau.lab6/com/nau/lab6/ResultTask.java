package com.nau.lab6;

import javafx.application.Platform;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class ResultTask implements Runnable {
    private double resultSquare = 0.0;
    private final ArrayList<RectangleTask> rectangleList;
    private final Text text;

    public ResultTask(Text t, ArrayList<RectangleTask> recTasks) {
        text = t;
        rectangleList = recTasks;
    }

    public double getResultSquare() {
        return resultSquare;
    }

    @Override
    public void run() {
        Runnable updater = () -> {
            double squareSum = 0.0;
            for (RectangleTask task : rectangleList) {
                squareSum += task.getSquare();
            }
            resultSquare = squareSum;
            text.setText("Общая площадь: " + squareSum + " пикселов");
        };

        while (true) {
            Platform.runLater(updater);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                System.out.println("Process is interrupted");
            }
        }
    }
}
