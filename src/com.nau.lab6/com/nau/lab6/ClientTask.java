package com.nau.lab6;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

public class ClientTask implements Runnable {
    private final String host;
    private final int port;
    private final Display display;
    private final Button connectButton;
    private final Button disconnectButton;
    private final ButtonsBlock[] buttonsBlocks;
    private final Title[] squares;
    private final Title squareSum;
    private Socket socket = null;

    public ClientTask(
        String host,
        String port,
        Display display,
        Button connectButton,
        Button disconnectButton,
        ButtonsBlock[] buttonsBlocks,
        Title[] squares,
        Title squareSum
    ) {
        this.host = host;
        this.port = Integer.parseInt(port);
        this.display = display;
        this.connectButton = connectButton;
        this.disconnectButton = disconnectButton;
        this.buttonsBlocks = buttonsBlocks;
        this.squares = squares;
        this.squareSum = squareSum;
    }

    public Socket getSocket() {
        return socket;
    }

    private void setupInitial(String[] response) {
        for (int i = 0; i < buttonsBlocks.length; i++) {
            ButtonsBlock block = buttonsBlocks[i];
            if (response[i + 1].equals(RectangleState.START.toString())) {
                block.getStart().setSelected(true);
                block.getStop().setSelected(false);
            } else {
                block.getStart().setSelected(false);
                block.getStop().setSelected(true);
            }
        }
    }

    private void updateSquareData(String[] response) {
        for (int i = 0; i < squares.length; i++) {
            squares[i].setText(response[i+1]);
        }
        squareSum.setText(response[5]);
    }

    @Override
    public void run() {
        try {
            socket = new Socket(host, port);
            Platform.runLater(() -> display.print(">>> Connected: " + host + ":" + port));
            OutputStream output = socket.getOutputStream();
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(output, true);
            writer.println(Constant.GET_INITIAL_DATA + ":");

            for (int i = 0; i < buttonsBlocks.length; i++) {
                ButtonsBlock block = buttonsBlocks[i];
                int finalI = i;
                block.getStart().setOnAction((ActionEvent event) -> {
                    writer.println(Constant.ON_BUTTON_ACTION + ":" + finalI + ":" + RectangleState.START);
                });
                block.getStop().setOnAction((ActionEvent event) -> {
                    writer.println(Constant.ON_BUTTON_ACTION + ":" + finalI + ":" + RectangleState.STOP);
                });
            }

            while (!socket.isClosed()) {
                String[] response = input.readLine().split(":");

                switch (response[0]) {
                    case Constant.GET_INITIAL_DATA -> setupInitial(response);
                    case Constant.GET_SQUARES -> updateSquareData(response);
                    default -> System.out.println("Unsupported!");
                }
            }
        } catch (SocketException ignore) {
        } catch (Exception e) {
            Platform.runLater(() -> display.print("== Connection failed: " + host + ":" + port));
            connectButton.setDisable(false);
            disconnectButton.setDisable(true);
        }
    }
}
