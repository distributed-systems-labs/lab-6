package com.nau.lab6;

import javafx.scene.text.Text;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

public class WorkerThread extends Thread {
    private final Socket socket;
    private final Text connections;
    private final Display display;
    private final ArrayList<Thread> threads;
    private final ArrayList<RectangleTask> rectangleTasks;
    private final ResultTask resultTask;

    public WorkerThread(
        Socket socket,
        Display display,
        Text connectionNumber,
        ArrayList<Thread> threads,
        ArrayList<RectangleTask> rectangleTasks,
        ResultTask resultTask
    ) {
        this.socket = socket;
        this.display = display;
        connections = connectionNumber;
        this.threads = threads;
        this.rectangleTasks = rectangleTasks;
        this.resultTask = resultTask;
    }

    private void incrementConnection() {
        int num = Integer.parseInt(connections.getText());
        connections.setText("" + (++num));
    }

    private void decrementConnection() {
        int num = Integer.parseInt(connections.getText());
        connections.setText("" + (--num));
    }

    private String getInitialData() {
        StringBuilder result = new StringBuilder(Constant.GET_INITIAL_DATA);
        for (RectangleTask task : rectangleTasks) {
            result.append(":" + task.getRectangle().getState());
        }
        return result.toString();
    }

    private void onButtonHandler(String[] request) {
        int threadIdx = Integer.parseInt(request[1]);
        String state = request[2];

        if (state.equals(RectangleState.START.toString())) {
            threads.get(threadIdx).resume();
            rectangleTasks.get(threadIdx).getRectangle().setState(RectangleState.START);
        } else {
            threads.get(threadIdx).suspend();
            rectangleTasks.get(threadIdx).getRectangle().setState(RectangleState.STOP);
        }
    }

    private String buildSquareResponse() {
        StringBuilder result = new StringBuilder(Constant.GET_SQUARES);
        for (RectangleTask task : rectangleTasks) {
            result.append(":").append(task.getSquare());
        }
        result.append(":").append(resultTask.getResultSquare());
        return result.toString();
    }

    @Override
    public void run() {
        display.print(">>> Connected: " + socket.getLocalPort());
        incrementConnection();
        BufferedReader inputStream;
        OutputStream outputStream;
        try {
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outputStream = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(outputStream, true);

            Thread sendDataThread = new Thread(() -> {
                while(true) {
                    try {
                        Thread.sleep(50);
                        writer.println(buildSquareResponse());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            sendDataThread.start();

            while (!socket.isClosed()) {
                byte firstSymbol = (byte) inputStream.read();
                if (firstSymbol == -1) {
                    display.print("<<< Disconnected: " + socket.getLocalPort());
                    decrementConnection();
                    socket.close();
                }
                String restInput = inputStream.readLine();
                if (restInput != null && !restInput.trim().equals("")) {
                    String input = new String(new byte[]{firstSymbol}) + restInput;
                    System.out.println("Input: " + input);
                    String[] request = input.split(":");

                    switch (request[0]) {
                        case Constant.GET_INITIAL_DATA -> writer.println(getInitialData());
                        case Constant.ON_BUTTON_ACTION -> onButtonHandler(request);
                        default -> System.out.println("Unsupported!");
                    }

                }
            }
            inputStream.close();
            sendDataThread.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

