package com.nau.lab6;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Title extends Text {
    public Title(String txt) {
        super(txt);
        this.setFill(Color.BLUE);
        this.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        this.setLayoutX(40);
        this.setLayoutY(30);
    }

    public Title(String txt, Color color) {
        super(txt);
        this.setFill(color);
        this.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        this.setLayoutX(40);
        this.setLayoutY(30);
    }

    public Title(String txt, Color color, FontWeight fontWeight) {
        super(txt);
        this.setFill(color);
        this.setFont(Font.font("Arial", fontWeight, 12));
        this.setLayoutX(40);
        this.setLayoutY(30);
    }
}
