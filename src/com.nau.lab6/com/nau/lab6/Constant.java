package com.nau.lab6;

public class Constant {
    public static final int PORT = 8000;
    public static final String HOST = "127.0.0.1";
    public static final String GET_INITIAL_DATA = "getInitialData";
    public static final String ON_BUTTON_ACTION = "onButtonAction";
    public static final String GET_SQUARES = "getSquares";
}
