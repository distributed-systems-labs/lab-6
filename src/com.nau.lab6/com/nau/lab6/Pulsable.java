package com.nau.lab6;

public interface Pulsable {
    public void pulse();
}
